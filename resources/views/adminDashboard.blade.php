 @include('common.application-common.header')

   


    <!-- Intro -->
    <div id="intro" class="basic-1">

        <div class="container">
            @if(Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
            @endif
            <div class="row" style="margin-top: 50px;">
               <table id="example" class="display"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>C_etc</th>
                        <th>E_etc </th>
                        <th>Notice Period</th>
                        <th>Action</th>
                    </tr>
                </thead>
         
                <tfoot>
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>C_etc</th>
                        <th>E_etc </th>
                        <th>Notice Period</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
         
                <tbody>
                    
                    @foreach($applicationStore as $applicationValue)
                        <tr>
                            <td>{{$applicationValue->id}}</td>
                            <td>{{$applicationValue->name}}</td>
                            <td>{{$applicationValue->phone}}</td>
                            <td>{{$applicationValue->email}}</td>
                            <td>{{$applicationValue->c_etc}}</td>
                            <td>{{$applicationValue->e_etc}}</td>
                            <td>{{$applicationValue->notice_period}}</td>
                            <td><a href="{{ route('editApplication',['id'=>$applicationValue->id]) }}">Edit</a></td>
                        </tr>
                    @endforeach
                    
                    
                </tbody>
            </table>
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-1 -->
    <!-- end of intro -->


    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">Copyright © 2020 <a href="https://inovatik.com">Template by Inovatik</a></p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright --> 
    <!-- end of copyright -->
    
        
    <!-- Scripts -->
        <script src="{{ asset('js/'.\Request::route()->getName()) }}.js"></script> 

    <script src="js/jquery.min.js"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="js/popper.min.js"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
    <script src="js/scripts.js"></script> <!-- Custom scripts -->
    <script src="https://cdn.datatables.net/1.10.1/js/jquery.dataTables.min.js"></script> <!-- Custom scripts -->
    <script src="https://cdn.datatables.net/responsive/1.0.0/js/dataTables.responsive.js"></script> <!-- Custom scripts -->
</body>
</html>

<script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable( {
                responsive: true
            } );
        } );

</script>