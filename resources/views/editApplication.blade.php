 @include('common.application-common.header')

   <!-- Intro -->
    <div id="intro" class="basic-1">
       <div class="container">
        <form action="{{ route('applicationUpdate') }}" enctype="multipart/form-data" method="post">
            @csrf
            <div  class="form-part">
            <h2>Simple Form Design</h2>
            @if(Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
            @endif
            <div class="form-inputs">
                  <div class="sqr-input">
                    <div class="text-input">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" value="{{($applicationGet) ? $applicationGet->name : ''}}" >
                        <div class="clearfix"></div>
                        @if ($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span> 
                        @endif
                    </div>

                    <div class="text-input">
                      <label for="phone">Mobile/Land line</label>
                      <input type="text" name="phone" id="phone" value="{{($applicationGet) ? $applicationGet->phone : ''}}">
                      <div class="clearfix"></div>
                        @if ($errors->has('phone'))
                            <span class="text-danger">{{ $errors->first('phone') }}</span> 
                        @endif
                    </div>
                    
                  </div>

              <div class="text-input">
                <label for="email">Email</label>
                <input type="text" name="email" id="email" value="{{($applicationGet) ? $applicationGet->email : ''}}"> 
                @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span> 
                @endif
              </div>

            <div class="text-input">
                <label for="country">Education Details</label>
            
                    <table class="table table-bordered" id="dynamicTable">  
                        <tr>
                            <th>Board</th>
                            <th>Year</th>
                            <th>Percentage</th>
                        </tr>
                        @foreach($educationDetails as $key => $educationDetailsV)
                        <tr>  
                            <td><input type="text" name="education[{{$key}}][board]" placeholder="Enter your board" class="form-control" value="{{$educationDetailsV->board}}" /></td>  
                            <td><input type="text" name="education[{{$key}}][year]" placeholder="Enter your year" class="form-control"  value="{{$educationDetailsV->year}}" /></td>  
                            <td><input type="text" name="education[{{$key}}][percentage]" placeholder="Enter your percentage" class="form-control" value="{{$educationDetailsV->percentage}}" /></td>  
                            @if($key == 0)
                                <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>
                            @else
                                <td><button type="button" class="btn btn-danger remove-tr">Remove</button></td>
                            @endif
                        </tr> 
                        @endforeach 
                            
                    </table> 


              </div>    
            </div>


            <div class="text-input">
                <label for="country">Technical Details</label>
                  <div class="table-responsive">
                    <table class="table table-bordered" id="techincalTable">
                        <tr>
                            <th>Company</th>
                            <th>designation</th>
                            <th>From</th>
                            <th>To</th>
                        </tr>
                        @foreach($experienceDetails as $key => $experienceDetailsV)
                        {{$experienceDetailsV->company}}
                            @php
                            $form = str_replace(' ','T',$experienceDetailsV->form);
                            $to_date = str_replace(' ','T',$experienceDetailsV->to_date);
                            @endphp
                        <tr>  
                            <td><input type="text" name="experience[{{$key}}][company]" placeholder="Enter your board" class="form-control" value="{{$experienceDetailsV->company}}" /></td>  
                            <td><input type="text" name="experience[{{$key}}][designation]" placeholder="Enter your year" class="form-control"  value="{{$experienceDetailsV->designation}}" /></td>  
                            <td><input type='datetime-local' name="experience[{{$key}}][form]" placeholder="Enter your year" class="form-control"  value="{{$form}}" /></td>  
                            <td><input type='datetime-local' name="experience[{{$key}}][to_date]" placeholder="Enter your year" class="form-control"  value="{{$to_date}}" /></td>  
                            @if($key == 0)
                                <td><button type="button" name="add" id="add-new" class="btn btn-success">Add More</button></td>  
                            @else
                                <td><button type="button" class="btn btn-danger remove-tr-new">Remove</button></td>
                            @endif

                        </tr> 
                        @endforeach 
                            
                    </table> 
                  </div>
                
            </div>  

            <div>

                @php
                    if(isset($langSet['Hindi'])){

                        $hindi = $langSet['Hindi'];
                        if(in_array('Write',$hindi)){
                            $isCheckedWriteHindi = 'checked="checked"';
                            $isDisableWriteHindi = '';
                        }else{
                            $isCheckedWriteHindi = '';
                            $isDisableWriteHindi = 'disabled="disabled"'; 
                        }  
                        if(in_array('Read',$hindi)){
                            $isCheckedReadHindi = 'checked="checked"';
                            $isDisableReadHindi = '';

                        }else{
                            $isCheckedReadHindi = '';
                            $isDisableReadHindi = 'disabled="disabled"'; 
                        } 
                        if(in_array('Speak',$hindi)){
                            $isCheckedSpeakHindi = 'checked="checked"';
                            $isDisableSpeakHindi = '';
                        }else{
                            $isCheckedSpeakHindi = '';
                            $isDisableSpeakHindi = 'disabled="disabled"'; 
                        }  
                    }else{
                        $isCheckedSpeakHindi = '';
                        $isDisableSpeakHindi = 'disabled="disabled"';

                        $isCheckedReadHindi = '';
                        $isDisableReadHindi = 'disabled="disabled"';

                        $isDisableWriteHindi = 'disabled="disabled"';
                        $isCheckedWriteHindi = '';
                    }
                @endphp
                <label for="e_ctc">Language Known</label>
                <br>
                 <input type="checkbox" name="Hindi" class="hindi-row"  value="Hindi"> : Hindi  
                <input type="checkbox" name="Hindi[]" class="hindi-read"  {{$isDisableReadHindi}} {{$isCheckedReadHindi}} value="Read">: Read
                <input type="checkbox" name="Hindi[]" class="hindi-write" {{$isDisableWriteHindi}} {{$isCheckedWriteHindi}}  value="Write">: Write 
                <input type="checkbox" name="Hindi[]" class="hindi-speak" {{$isDisableSpeakHindi}} {{$isCheckedSpeakHindi}} value="Speak">: Speak
                <br>

                @php
                    if(isset($langSet['English'])){

                        $english = $langSet['English'];
                        if(in_array('Write',$english)){
                            $isCheckedWriteEnglish = 'checked="checked"';
                            $isDisableWriteEnglish = '';
                        }else{
                            $isCheckedWriteEnglish = '';
                            $isDisableWriteEnglish = 'disabled="disabled"'; 
                        }  
                        if(in_array('Read',$english)){
                            $isCheckedReadEnglish = 'checked="checked"';
                            $isDisableReadEnglish = '';

                        }else{
                            $isCheckedReadEnglish = '';
                            $isDisableReadEnglish = 'disabled="disabled"'; 
                        } 
                        if(in_array('Speak',$english)){
                            $isCheckedSpeakEnglish = 'checked="checked"';
                            $isDisableSpeakEnglish = '';
                        }else{
                            $isCheckedSpeakEnglish = '';
                            $isDisableSpeakEnglish = 'disabled="disabled"'; 
                        }  
                    }else{
                        $isCheckedSpeakEnglish = '';
                        $isDisableSpeakEnglish = 'disabled="disabled"';

                        $isCheckedReadEnglish = '';
                        $isDisableReadEnglish = 'disabled="disabled"';

                        $isDisableWriteEnglish = 'disabled="disabled"';
                        $isCheckedWriteEnglish = '';
                    }
                @endphp

                <input type="checkbox" name="English" class="english-row"   value="English"> : English 
                <input type="checkbox" name="English[]" class="english-read" {{$isCheckedReadEnglish}} {{$isDisableReadEnglish}} value="Read">: Read
                <input type="checkbox" name="English[]" class="english-write" {{$isCheckedWriteEnglish}} {{$isDisableWriteEnglish}} value="Write">: Write
                <input type="checkbox" name="English[]" class="english-speak" {{$isDisableSpeakEnglish}} {{$isCheckedSpeakEnglish}} value="Speak">: Speak
                <br>

                @php
                    if(isset($langSet['Gujarati'])){


                        $gujarati = $langSet['Gujarati'];
                        if(in_array('Write',$gujarati)){
                            $isCheckedWriteGujarati = 'checked="checked"';
                            $isDisableWriteGujarati = '';
                        }else{
                            $isCheckedWriteGujarati = '';
                            $isDisableWriteGujarati = 'disabled="disabled"'; 
                        }  
                        if(in_array('Read',$gujarati)){
                            $isCheckedReadGujarati = 'checked="checked"';
                            $isDisableReadGujarati = '';

                        }else{
                            $isCheckedReadGujarati = '';
                            $isDisableReadGujarati = 'disabled="disabled"'; 
                        } 
                        if(in_array('Speak',$gujarati)){
                            $isCheckedSpeakGujarati = 'checked="checked"';
                            $isDisableSpeakGujarati = '';
                        }else{
                            $isCheckedSpeakGujarati = '';
                            $isDisableSpeakGujarati = 'disabled="disabled"'; 
                        }  
                    }else{
                        $isCheckedSpeakGujarati = '';
                            $isDisableSpeakGujarati = 'disabled="disabled"'; 

                        $isCheckedReadGujarati = '';
                            $isDisableReadGujarati = 'disabled="disabled"'; 

                            $isDisableWriteGujarati = 'disabled="disabled"'; 
                        $isCheckedWriteGujarati = '';
                    }
                @endphp

                <input type="checkbox" name="Gujarati" class="gujarati-row"   value="Gujarati"> :Gujarati  
                <input type="checkbox" name="Gujarati[]" class="gujarati-read" {{$isDisableReadGujarati}} {{$isCheckedReadGujarati}} value="Read">: Read
                <input type="checkbox" name="Gujarati[]" class="gujarati-write" {{$isDisableWriteGujarati}} {{$isCheckedWriteGujarati}} value="Write">: Write
                <input type="checkbox" name="Gujarati[]" class="gujarati-speak" {{$isDisableSpeakGujarati}} {{$isCheckedSpeakGujarati}} value="Speak">: Speak
            </div>

            </div>

            <div style="margin-top: 50px; margin-bottom: 50px;">

                <label for="e_ctc">Techincal Exp.</label>
                <br>
                 @php
                    if(isset($technicaSet['PHP'])){

                        $PHP = $technicaSet['PHP'];
                        if(in_array('Expert',$PHP)){
                            $isCheckedExpertPHP = 'checked="checked"';
                            $isDisableExpertPHP = '';
                        }else{
                            $isCheckedExpertPHP = '';
                            $isDisableExpertPHP = 'disabled="disabled"'; 
                        }  
                        if(in_array('Mediator',$PHP)){
                            $isCheckedMediatorPHP = 'checked="checked"';
                            $isDisableMediatorPHP = '';

                        }else{
                            $isCheckedMediatorPHP = '';
                            $isDisableMediatorPHP = 'disabled="disabled"'; 
                        } 
                        if(in_array('Beginer',$PHP)){
                            $isCheckedBeginerPHP = 'checked="checked"';
                            $isDisableBeginerPHP = '';
                        }else{
                            $isCheckedBeginerPHP = '';
                            $isDisableBeginerPHP = 'disabled="disabled"'; 
                        }  
                    }else{
                        $isCheckedBeginerPHP = '';
                        $isDisableBeginerPHP = 'disabled="disabled"';

                        $isCheckedMediatorPHP = '';
                        $isDisableMediatorPHP = 'disabled="disabled"';

                        $isDisableExpertPHP = 'disabled="disabled"';
                        $isCheckedExpertPHP = '';
                    }
                @endphp
                <input type="checkbox"  class="php"  value="PHP"> : PHP  
                <input type="radio" name="techincal['PHP']" class="php-beg" {{$isCheckedBeginerPHP}} {{$isDisableBeginerPHP}}  value="Beginer">: Beginer
                <input type="radio" name="techincal['PHP']" class="php-med" {{$isCheckedMediatorPHP}} {{$isDisableMediatorPHP}}  value="Mediator">: Mediator 
                <input type="radio" name="techincal['PHP']" class="php-exp" {{$isDisableExpertPHP}} {{$isCheckedExpertPHP}}  value="Expert">: Expert
                <br>

                @php
                    if(isset($technicaSet['Mysql'])){

                        $Mysql = $technicaSet['Mysql'];
                        
                        if(in_array('Expert',$Mysql)){
                            $isCheckedExpertMysql = 'checked="checked"';
                            $isDisableExpertMysql = '';
                        }else{
                            $isCheckedExpertMysql = '';
                            $isDisableExpertMysql = 'disabled="disabled"'; 
                        }  
                        if(in_array('Mediator',$Mysql)){
                            $isCheckedMediatorMysql = 'checked="checked"';
                            $isDisableMediatorMysql = '';

                        }else{
                            $isCheckedMediatorMysql = '';
                            $isDisableMediatorMysql = 'disabled="disabled"'; 
                        } 
                        if(in_array('Beginer',$Mysql)){
                            $isCheckedBeginerMysql= 'checked="checked"';
                            $isDisableBeginerMysql = '';
                        }else{
                            $isCheckedBeginerMysql = '';
                            $isDisableBeginerMysql = 'disabled="disabled"'; 
                        }  
                    }else{
                        $isCheckedBeginerMysql = '';
                        $isDisableBeginerMysql= 'disabled="disabled"';

                        $isCheckedMediatorMysql = '';
                        $isDisableMediatorMysql = 'disabled="disabled"';

                        $isDisableExpertMysql = 'disabled="disabled"';
                        $isCheckedExpertMysql = '';
                    }
                @endphp
                <input type="checkbox"  class="mysql"   value="English"> : Mysql 
                <input type="radio" name="techincal['Mysql']" class="mysql-beg"  {{$isCheckedBeginerMysql}} {{$isDisableBeginerMysql}} value="Beginer">: Beginer
                <input type="radio" name="techincal['Mysql']" class="mysql-med" {{$isDisableMediatorMysql}} {{$isCheckedMediatorMysql}}  value="Mediator">: Mediator 
                <input type="radio" name="techincal['Mysql']" class="mysql-exp" {{$isDisableExpertMysql}} {{$isCheckedExpertMysql}} value="Expert">: Expert
                <br>

                @php
                    if(isset($technicaSet['Laravel'])){

                        $Laravel = $technicaSet['Laravel'];
                        if(in_array('Expert',$Laravel)){
                            $isCheckedExpertLaravel = 'checked="checked"';
                            $isDisableExpertLaravel = '';
                        }else{
                            $isCheckedExpertLaravel = '';
                            $isDisableExpertLaravel = 'disabled="disabled"'; 
                        }  
                        if(in_array('Mediator',$Laravel)){
                            $isCheckedMediatorLaravel = 'checked="checked"';
                            $isDisableMediatorLaravel = '';

                        }else{
                            $isCheckedMediatorLaravel = '';
                            $isDisableMediatorLaravel = 'disabled="disabled"'; 
                        } 
                        if(in_array('Beginer',$Laravel)){
                            $isCheckedBeginerLaravel= 'checked="checked"';
                            $isDisableBeginerLaravel = '';
                        }else{
                            $isCheckedBeginerLaravel = '';
                            $isDisableBeginerLaravel = 'disabled="disabled"'; 
                        }  
                    }else{
                        $isCheckedBeginerLaravel = '';
                        $isDisableBeginerLaravel= 'disabled="disabled"';

                        $isCheckedMediatorLaravel = '';
                        $isDisableMediatorLaravel = 'disabled="disabled"';

                        $isDisableExpertLaravel = 'disabled="disabled"';
                        $isCheckedExpertLaravel = '';
                    }
                @endphp
                 <input type="checkbox"  class="laravel"   value="Laravel"> :Laravel  
                <input type="radio" name="techincal['Laravel']"  class="laravel-beg"  {{$isDisableBeginerLaravel}} {{$isCheckedBeginerLaravel}} value="Beginer">: Beginer
                <input type="radio" name="techincal['Laravel']"  class="laravel-med" {{$isDisableMediatorLaravel}} {{$isCheckedMediatorLaravel}}  value="Mediator">: Mediator 
                <input type="radio" name="techincal['Laravel']"  class="laravel-exp" {{$isDisableExpertLaravel}} {{$isCheckedExpertLaravel}} value="Expert">: Expert
                <br>

                @php
                    if(isset($technicaSet['Oracle'])){

                        $Oracle = $technicaSet['Oracle'];
                        if(in_array('Expert',$Oracle)){
                            $isCheckedExpertOracle = 'checked="checked"';
                            $isDisableExpertOracle = '';
                        }else{
                            $isCheckedExpertOracle = '';
                            $isDisableExpertOracle = 'disabled="disabled"'; 
                        }  
                        if(in_array('Mediator',$Oracle)){
                            $isCheckedMediatorOracle = 'checked="checked"';
                            $isDisableMediatorOracle = '';

                        }else{
                            $isCheckedMediatorOracle = '';
                            $isDisableMediatorOracle = 'disabled="disabled"'; 
                        } 
                        if(in_array('Beginer',$Oracle)){
                            $isCheckedBeginerOracle= 'checked="checked"';
                            $isDisableBeginerOracle = '';
                        }else{
                            $isCheckedBeginerOracle = '';
                            $isDisableBeginerOracle = 'disabled="disabled"'; 
                        }  
                    }else{
                        $isCheckedBeginerOracle = '';
                        $isDisableBeginerOracle= 'disabled="disabled"';

                        $isCheckedMediatorOracle = '';
                        $isDisableMediatorOracle = 'disabled="disabled"';

                        $isDisableExpertOracle = 'disabled="disabled"';
                        $isCheckedExpertOracle = '';
                    }
                @endphp
                <input type="checkbox" class="oracle"   value="Gujarati"> :Oracle  
                <input type="radio" name="techincal['Oracle']" class="oracle-beg"  {{$isCheckedBeginerOracle}} {{$isDisableBeginerOracle}} value="Beginer">: Beginer
                <input type="radio" name="techincal['Oracle']" class="oracle-med" {{$isCheckedMediatorOracle}} {{$isDisableMediatorOracle}}  value="Mediator">: Mediator 
                <input type="radio" name="techincal['Oracle']" class="oracle-exp" {{$isDisableExpertOracle}} {{$isCheckedExpertOracle}} value="Expert">: Expert
            </div>

            <div class="sqr-input">
                <div class="text-input">
                    <label for="p_location">Preferred Location</label>
                    @php
                        $locationGet = explode(',',$applicationGet->prefered_location) ;
                        $prefered_location =  array(
                            'Ahmedabad' => 'Ahmedabad',
                            'Mumbai' => 'Mumbai',
                            'Surat' => 'Surat',
                            'Delhi' => 'Delhi',

                        );
                    @endphp
                     <select multiple="multiple" class="form-control select-custom"  name="prefered_location[]" id="prefered_location"  data-placeholder="Select floor plans">
                        @foreach ($prefered_location as $key => $locationGetV)

                            <option <?php echo (in_array($key,$locationGet) ? 'selected' : '')  ?> value="{{$key}}" 
                                 > {{$locationGetV}} 
                            </option>
                        @endforeach
                    </select> 
                    <div class="clearfix"></div>
                    @if ($errors->has('p_location'))
                        <span class="text-danger">{{ $errors->first('p_location') }}</span> 
                    @endif
                </div>

                <div class="text-input">
                  <label for="e_ctc">Expected CTC</label>
                  <input type="text" name="e_ctc" id="e_ctc" value="{{($applicationGet) ? $applicationGet->e_ctc : ''}}">
                  <div class="clearfix"></div>
                    @if ($errors->has('e_ctc'))
                        <span class="text-danger">{{ $errors->first('e_ctc') }}</span> 
                    @endif
                </div>
                
            </div>  
            <div class="sqr-input">
                <div class="text-input">
                    <label for="c_ctc">Current CTC</label>
                    <input type="text" name="c_ctc" id="c_ctc" value="{{($applicationGet) ? $applicationGet->c_ctc : ''}}">
                    <div class="clearfix"></div>
                    @if ($errors->has('c_ctc'))
                        <span class="text-danger">{{ $errors->first('c_ctc') }}</span> 
                    @endif
                </div>

                <div class="text-input">
                  <label for="notice">Notice Period (In months)</label>
                  <input type="text" name="notice" id="notice" value="{{($applicationGet) ? $applicationGet->notice_period : ''}}">
                  <div class="clearfix"></div>
                    @if ($errors->has('notice'))
                        <span class="text-danger">{{ $errors->first('notice') }}</span> 
                    @endif
                </div>
                
            </div>  
              

                <div class="custom-btn" >
                  <div class="text-input">
                    <input type="hidden" name="id" id="id" value="{{($applicationGet) ? $applicationGet->id : ''}}">
                    <input type="submit" id="submit" value="submit">
                  </div>
                </div>
            </div>
              
               
            

          </div>
        </form>
        </div>
    </div> <!-- end of basic-1 -->
    <!-- end of intro -->

    <!-- Footer -->
    @include('common.application-common.footer')


    