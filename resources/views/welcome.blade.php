    @include('common.application-common.header')

    <!-- Intro -->
    <div id="intro" class="basic-1">
       <div class="container">
        <form action="{{ route('applicationStore') }}" enctype="multipart/form-data" method="post">
            @csrf
            <div  class="form-part">
            <h2>Simple Form Design</h2>
            @if(Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
            @endif
            <div class="form-inputs">
                  <div class="sqr-input">
                    <div class="text-input">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" >
                        <div class="clearfix"></div>
                        @if ($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span> 
                        @endif
                    </div>

                    <div class="text-input">
                      <label for="phone">Mobile/Land line</label>
                      <input type="text" name="phone" id="phone">
                      <div class="clearfix"></div>
                        @if ($errors->has('phone'))
                            <span class="text-danger">{{ $errors->first('phone') }}</span> 
                        @endif
                    </div>
                    
                  </div>

              <div class="text-input">
                <label for="email">Email</label>
                <input type="text" name="email" id="email">
                @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span> 
                @endif
              </div>

            <div class="text-input">
                <label for="country">Education Details</label>
                <table class="table table-bordered" id="educationTable">  
                    <tr>
                        <th>Board</th>
                        <th>Year</th>
                        <th>Percentage</th>
                    </tr>
                    <tr>  
                        <td><input type="text" name="education[0][board]" placeholder="Enter your board" class="form-control" /></td>  
                        <td><input type="text" name="education[0][year]" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" placeholder="Enter your year" class="form-control" /></td>  
                        <td><input type="text" name="education[0][percentage]" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" placeholder="Enter your percentage" class="form-control" /></td>  
                        <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
                    </tr>  
                </table> 
            </div>


            <div class="text-input">
                <label for="Techincal">Experience Details</label>
                    <table class="table table-bordered" id="techincalTable">
                    <tr>
                          <th>Company</th>
                          <th>Designation</th>
                          <th>From</th>
                          <th>To</th>
                     </tr>
                    <tr>
                        <td><input type="text" name="experience[0][company]" placeholder="Enter your company" class="form-control" /></td>  
                        <td><input type="text" name="experience[0][designation]" placeholder="Enter your designation" class="form-control" /></td> 
                        <td><input type="datetime-local" name="experience[0][form]" placeholder="Enter your form date" class="form-control" /></td>
                        <td><input type="datetime-local" name="experience[0][to_date]" placeholder="Enter your to date" class="form-control" /></td>
                        <td><button type="button" name="add" id="add-new" class="btn btn-success">Add More</button></td>  
                    </tr>
                    </table>
                    <!-- <input id='add-row' class='btn btn-primary' type='button' value='Add' /> -->
                
            </div>  

            <div>

                <label for="e_ctc">Language Known</label>
                <br>
                 <input type="checkbox" name="Hindi" class="hindi-row"  value="Hindi"> : Hindi  
                <input type="checkbox" name="Hindi[]" class="hindi-read"  disabled="disabled" value="Read">: Read
                <input type="checkbox" name="Hindi[]" class="hindi-write" disabled="disabled"  value="Write">: Write 
                <input type="checkbox" name="Hindi[]" class="hindi-speak" disabled="disabled" value="Speak">: Speak
                <br>
                <input type="checkbox" name="English" class="english-row"   value="English"> : English 
                <input type="checkbox" name="English[]" class="english-read" disabled="disabled" value="Read">: Read
                <input type="checkbox" name="English[]" class="english-write" disabled="disabled" value="Write">: Write
                <input type="checkbox" name="English[]" class="english-speak" disabled="disabled" value="Speak">: Speak
                <br>
                 <input type="checkbox" name="Gujarati" class="gujarati-row"   value="Gujarati"> :Gujarati  
                <input type="checkbox" name="Gujarati[]" class="gujarati-read" disabled="disabled" value="Read">: Read
                <input type="checkbox" name="Gujarati[]" class="gujarati-write" disabled="disabled" value="Write">: Write
                <input type="checkbox" name="Gujarati[]" class="gujarati-speak" disabled="disabled" value="Speak">: Speak
            </div>

            <div style="margin-top: 50px; margin-bottom: 50px;">

                <label for="e_ctc">Techincal Exp.</label>
                <br>
                <input type="checkbox"  class="php"  value="PHP"> : PHP  
                <input type="radio" name="techincal['PHP']" class="php-beg"  disabled="disabled" value="Beginer">: Beginer
                <input type="radio" name="techincal['PHP']" class="php-med" disabled="disabled"  value="Mediator">: Mediator 
                <input type="radio" name="techincal['PHP']" class="php-exp" disabled="disabled" value="Expert">: Expert
                <br>
                <input type="checkbox"  class="mysql"   value="English"> : Mysql 
                <input type="radio" name="techincal['Mysql']" class="mysql-beg"  disabled="disabled" value="Beginer">: Beginer
                <input type="radio" name="techincal['Mysql']" class="mysql-med" disabled="disabled"  value="Mediator">: Mediator 
                <input type="radio" name="techincal['Mysql']" class="mysql-exp" disabled="disabled" value="Expert">: Expert
                <br>
                 <input type="checkbox"  class="laravel"   value="Gujarati"> :Laravel  
                <input type="radio" name="techincal['Laravel']" class="laravel-beg"  disabled="disabled" value="Beginer">: Beginer
                <input type="radio" name="techincal['Laravel']" class="laravel-med" disabled="disabled"  value="Mediator">: Mediator 
                <input type="radio" name="techincal['Laravel']" class="laravel-exp" disabled="disabled" value="Expert">: Expert
                <br>
                <input type="checkbox" class="oracle"   value="Gujarati"> :Oracle  
                <input type="radio" name="techincal['Oracle']" class="oracle-beg"  disabled="disabled" value="Beginer">: Beginer
                <input type="radio" name="techincal['Oracle']" class="oracle-med" disabled="disabled"  value="Mediator">: Mediator 
                <input type="radio" name="techincal['Oracle']" class="oracle-exp" disabled="disabled" value="Expert">: Expert
            </div>

            <div class="sqr-input">
                <div class="text-input">
                    <label for="p_location">Preferred Location</label>
                    @php

                        $prefered_location =  array(
                            'Ahmedabad' => 'Ahmedabad',
                            'Mumbai' => 'Mumbai',
                            'Surat' => 'Surat',
                            'Delhi' => 'Delhi',

                        );
                    @endphp
                     <select multiple="multiple" class="form-control select-custom"  name="prefered_location[]" id="prefered_location"  data-placeholder="Select floor plans">
                        @foreach ($prefered_location as $key => $prefered_locationV)
                            <option value="{{$key}}" 
                                 > {{$prefered_locationV}} 
                            </option>
                        @endforeach
                    </select> 
                    <div class="clearfix"></div>
                    @if ($errors->has('p_location'))
                        <span class="text-danger">{{ $errors->first('p_location') }}</span> 
                    @endif
                </div>

                <div class="text-input">
                  <label for="e_ctc">Expected CTC</label>
                  <input type="text" name="e_ctc" id="e_ctc">
                  <div class="clearfix"></div>
                    @if ($errors->has('e_ctc'))
                        <span class="text-danger">{{ $errors->first('e_ctc') }}</span> 
                    @endif
                </div>
                
            </div>  
            <div class="sqr-input">
                <div class="text-input">
                    <label for="c_ctc">Current CTC</label>
                    <input type="text" name="c_ctc" id="c_ctc" >
                    <div class="clearfix"></div>
                    @if ($errors->has('c_ctc'))
                        <span class="text-danger">{{ $errors->first('c_ctc') }}</span> 
                    @endif
                </div>

                <div class="text-input">
                  <label for="notice">Notice Period</label>
                  <input type="text" name="notice" id="notice">
                  <div class="clearfix"></div>
                    @if ($errors->has('notice'))
                        <span class="text-danger">{{ $errors->first('notice') }}</span> 
                    @endif
                </div>
                
            </div>  
              

                <div class="custom-btn" >
                  <div class="text-input">
                    <input type="submit" id="submit" value="submit">
                  </div>
                </div>
            </div>
              
               
            

          </div>
        </form>
        </div>
    </div> <!-- end of basic-1 -->
    <!-- end of intro -->

    <!-- Footer -->
    
    @include('common.application-common.footer')
