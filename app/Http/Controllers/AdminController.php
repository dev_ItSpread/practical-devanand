<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Http\Requests\AdminLoginRequest;
use Auth;
use Session;
use Redirect;
use App\Models\ApplicationMaster;


class AdminController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(Request $request){

    	return view('login');
    }

    public function login(AdminLoginRequest $request){
    	$userdata = array(
          'email' => $request->input('email'),
          'password' => $request->input('password')
        );
        // attempt to do the login
        if (Auth::attempt($userdata))
	      {
	      // validation successful
	      // do whatever you want on success
            Session::put('userDetails', $userdata);
	      	return Redirect::to('adminDashboard');
	      }
	      else
	      {
	      // validation not successful, send back to form
	      	return Redirect::to('adminLogin');
	      }
    }

    public function adminDashboard(Request $request){
        
        $applicationStore = ApplicationMaster::get();
    	return view('adminDashboard',compact('applicationStore'));
    }
}
