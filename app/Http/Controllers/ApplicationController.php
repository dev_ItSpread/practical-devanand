<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Http\Requests\ApplicationStore;
use Auth;
use Redirect;

use App\Models\ApplicationMaster;
use App\Models\EducationDetail;
use App\Models\ExperienceDetail;
use App\Models\LanguageKnown;
use App\Models\TechnicalExp;
use DB;
class ApplicationController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sessionCheck(){

    }
    public function index(ApplicationStore $request){
        
            // dd($request->input('Hindi'));
        $locations = ($request->input('prefered_location') != '' ) ? implode(',', $request->input('prefered_location')) : '';
        $applicationArray = array(
            'name' =>$request->input('name'),
            'phone' =>$request->input('phone'),
            'email' => $request->input('email'),
            'prefered_location' => $locations,
            'c_ctc' => $request->input('c_ctc'),
            'e_ctc' => $request->input('e_ctc'),
            'notice_period' => $request->input('notice'),

            );
        $applicationStore = ApplicationMaster::create($applicationArray);
        $insertedId = $applicationStore->id;


        if(!empty($request->input('techincal'))){
            $tech = $request->input('techincal');
            foreach ($tech as $key => $techV) {
                print_r($techV);
                $techData = array(
                    'tech_name' =>str_replace("'", "", $key),
                    'tech_status' =>$techV,
                    'application_id' =>$insertedId
                );
                TechnicalExp::create($techData);
            }
        }
        if(!empty($request->input('education'))){
            $education  = $request->input('education');
            foreach ($education as $key => $educationV) {
                $educationData = array(
                    'board' => $educationV['board'],
                    'year' => $educationV['year'],
                    'percentage' => $educationV['percentage'],
                    'application_id' =>$insertedId
                );
                EducationDetail::create($educationData);
            }
        }

        if(!empty($request->input('experience'))){
                $experience  = $request->input('experience');
                foreach ($experience as $key => $experienceV) {
                $techData = array(
                    'company' => $experienceV['company'],
                    'designation' => $experienceV['designation'],
                    'form' => $experienceV['form'],
                    'to_date' => $experienceV['to_date'],
                    'application_id' =>$insertedId
                );
                ExperienceDetail::create($techData);
            }
        }


        if(!empty($request->input('Hindi'))){
            $hindiArray  = $request->input('Hindi');
            foreach ($hindiArray as $key => $hindiArrayV) {
                $hindiLang = array(
                    'language_name' =>'Hindi',
                    'lang_status' =>$hindiArrayV,
                    'application_id' =>$insertedId
                );
                LanguageKnown::create($hindiLang);
            }
        }

        if(!empty($request->input('English'))){
            $englishArray  = $request->input('English');
            // array_shift($englishArray);
            foreach ($englishArray as $key => $englishArrayV) {
                $engLang = array(
                    'language_name' =>'English',
                    'lang_status' =>$englishArrayV,
                    'application_id' =>$insertedId
                );
                LanguageKnown::create($engLang);
            }
        }

        if(!empty($request->input('Gujarati'))){
            $gujArray  = $request->input('Gujarati');
            // array_shift($gujArray);
            foreach ($gujArray as $key => $gujArrayV) {
                $gujLang = array(
                    'language_name' =>'Gujarati',
                    'lang_status' =>$gujArrayV,
                    'application_id' =>$insertedId
                );
                LanguageKnown::create($gujLang);
            }
        }

        if($request->input('PHP') != ''){
            $phpExp = array(
                'tech_name' =>'PHP',
                'tech_status' =>$request->input('PHP'),
                'application_id' =>$insertedId
            );
            TechnicalExp::create($phpExp);
        }


        if($request->input('Mysql') != ''){
            $phpExp = array(
                'tech_name' =>'Mysql',
                'tech_status' =>$request->input('Mysql'),
                'application_id' =>$insertedId
            );
            TechnicalExp::create($phpExp);
        }

        if($request->input('Laravel') != ''){
            $phpExp = array(
                'tech_name' =>'Laravel',
                'tech_status' =>$request->input('Laravel'),
                'application_id' =>$insertedId
            );
            TechnicalExp::create($phpExp);
        }

         if($request->input('Oracle') != ''){
            $phpExp = array(
                'tech_name' =>'Oracle',
                'tech_status' =>$request->input('Oracle'),
                'application_id' =>$insertedId
            );
            TechnicalExp::create($phpExp);
        }

        $request->session()->flash('success', 'Record successfully added!');
        return Redirect::back();

    }

    public function editApp( Request $request){
        $id = $request->input('id');
        // $applicationStore = ApplicationMaster::where('id',$id)->first();

        $applicationGet = ApplicationMaster::where('id',$id)->first();
         
        $educationDetails = DB::table('application_master as AM')
            ->leftJoin('education_details as ED', 'AM.id', '=', 'ED.application_id')
            ->where('AM.id',$id)->get();   

        $experienceDetails = DB::table('application_master as AM')
            ->leftJoin('experience_details as EXD', 'AM.id', '=', 'EXD.application_id')
            ->where('AM.id',$id)->get();

        $languageKnown = DB::table('application_master as AM')
            ->leftJoin('language_known as LK', 'AM.id', '=', 'LK.application_id')
            ->where('AM.id',$id)->get();
        $langSet = [];
        if(count($languageKnown) > 0){
            foreach ($languageKnown as $key => $languageKnownV) {
                $langSet[$languageKnownV->language_name][] = $languageKnownV->lang_status;
            }
        }

        
        $technicalExp = DB::table('application_master as AM')
            ->leftJoin('technical_exp as TE', 'AM.id', '=', 'TE.application_id')
            ->where('AM.id',$id)->get();
        $technicaSet = [];
        if(count($technicalExp) > 0){
            foreach ($technicalExp as $key => $technicalExpV) {
                $technicaSet[$technicalExpV->tech_name][] = $technicalExpV->tech_status;
            }
        }

        return view('editApplication',compact('applicationGet','educationDetails','experienceDetails','languageKnown','technicalExp','langSet','technicaSet'));
    }

    public function applicationUpdate(Request $request){
        
        $id = $request->input('id');
        
        
        $locations = ($request->input('prefered_location') != '' ) ? implode(',', $request->input('prefered_location')) : '';
        $applicationArray = array(
            'name' =>$request->input('name'),
            'phone' =>$request->input('phone'),
            'email' => $request->input('email'),
            'prefered_location' => $locations,
            'c_ctc' => $request->input('c_ctc'),
            'e_ctc' => $request->input('e_ctc'),
            'notice_period' => $request->input('notice'),

            );
        $applicationUpdate = DB::table('application_master')->where('id',$id)->update($applicationArray);

        if(!empty($request->input('Hindi'))){
            $applicationStore = DB::table('language_known')->where('language_name','Hindi')->where('application_id',$id)->delete();
            $hindiArray  = $request->input('Hindi');
            foreach ($hindiArray as $key => $hindiArrayV) {
                $hindiLang = array(
                    'language_name' =>'Hindi',
                    'lang_status' =>$hindiArrayV,
                    'application_id' =>$id
                );
                LanguageKnown::create($hindiLang);
            }
        }


        if(!empty($request->input('education'))){
            $education_details_delete = DB::table('education_details')->where('application_id',$id)->delete();
            $education  = $request->input('education');
            foreach ($education as $key => $educationV) {
                $educationData = array(
                    'board' => $educationV['board'],
                    'year' => $educationV['year'],
                    'percentage' => $educationV['percentage'],
                    'application_id' =>$id
                );
                EducationDetail::create($educationData);
            }
        }

        if(!empty($request->input('experience'))){

            $experience_details_delete = DB::table('experience_details')->where('application_id',$id)->delete();
            $experience  = $request->input('experience');
            foreach ($experience as $key => $experienceV) {
                $experienceVData = array(
                    'company' => $experienceV['company'],
                    'designation' => $experienceV['designation'],
                    'form' => $experienceV['form'],
                    'to_date' => $experienceV['to_date'],
                    'application_id' =>$id
                );
                ExperienceDetail::create($experienceVData);
            }
        }

        if(!empty($request->input('techincal'))){
            $tech = $request->input('techincal');
            $technical_exp_delete = DB::table('technical_exp')->where('application_id',$id)->delete();
            foreach ($tech as $key => $techV) {
                $techData = array(
                    'tech_name' =>str_replace("'", "", $key),
                    'tech_status' =>$techV,
                    'application_id' =>$id
                );
                TechnicalExp::create($techData);
            }
        }      

         return redirect('/adminDashboard')->with('success', 'Record updated successfully!');
        // $request->session()->flash('success', 'Record updated successfully!');
        // return Redirect::back();

    }


    public function home(){
        return view('welcome');
    }


    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

}
