<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplicationStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'notice' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'integer'],
            'e_ctc' => ['required', 'integer'],
            'c_ctc' => ['required', 'integer'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ];
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    // public function messages()
    // {
    //     return [
    //         'name.required' => 'Password is required!',
    //         'phone.required' => 'Password is required!',
    //         'email.required' => 'Password is required!'
    //     ];
    // }
}
