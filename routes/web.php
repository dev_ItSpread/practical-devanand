<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('applicationHome');


Route::get('/', 'App\Http\Controllers\ApplicationController@home')->name('applicationHome');
Route::get('/adminLogin', 'App\Http\Controllers\AdminController@index')->name('admin-login');
Route::post('/login', 'App\Http\Controllers\AdminController@login')->name('login');
Route::get('/adminDashboard/', 'App\Http\Controllers\AdminController@adminDashboard')->name('adminDashboard');
Route::post('/applicationStore', 'App\Http\Controllers\ApplicationController@index')->name('applicationStore');
Route::post('/applicationUpdate', 'App\Http\Controllers\ApplicationController@applicationUpdate')->name('applicationUpdate');
Route::get('/editApplication', 'App\Http\Controllers\ApplicationController@editApp')->name('editApplication');
Route::get('/logout', 'App\Http\Controllers\ApplicationController@logout')->name('logout');


    