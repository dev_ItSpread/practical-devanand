const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.styles([
    'public/css/bootstrap.css',
    'public/css/fontawesome-all.css',
    'public/css/magnific-popup.css',
    'public/css/styles.css',
    'public/css/swiper.css',
    'public/css/bootstrap.min.css',
    'public/css/adminDashboardAll.css',
    'public/css/jquery.dataTables.css',
    'public/css/dataTables.responsive.css',
], 'public/css/adminDashboard.css');

mix.styles([
    'public/js/jquery.min.js',
    'public/js/popper.min.js',
    'public/js/swiper.min.js',
    'public/js/scripts.js',
    'public/js/jquery.dataTables.min.js',
    'public/js/dataTables.responsive.js',
], 'public/js/adminDashboard.js');


mix.styles([
    'public/css/bootstrap.css',
    'public/css/fontawesome-all.css',
    'public/css/magnific-popup.css',
    'public/css/styles.css',
    'public/css/swiper.css',
    'public/css/bootstrap.min.css',
    'public/css/select2.min.css',
    'public/css/select2-bootstrap.min.css',
], 'public/css/applicationHome.css');




mix.styles([
    'public/js/jquery.min.js',
    'public/js/popper.min.js',
    'public/js/bootstrap.min.js',
    'public/js/jquery.easing.min.js',
    'public/js/swiper.min.js',
    'public/js/jquery.magnific-popup.js',
    'public/js/morphext.min.js',
    'public/js/isotope.pkgd.min.js',
    'public/js/validator.min.js',
    'public/js/scripts.js',
    'public/js/select2.min.js',
    'public/js/select2tree.js',
    'public/js/home-extra.js',
], 'public/js/applicationHome.js');

mix.styles([
    'public/css/bootstrap.css',
    'public/css/fontawesome-all.css',
    'public/css/magnific-popup.css',
    'public/css/styles.css',
    'public/css/swiper.css',
     'public/css/bootstrap.min.css',
    'public/css/select2.min.css',
    'public/css/select2-bootstrap.min.css',
], 'public/css/editApplication.css');

mix.styles([
    'public/js/jquery.min.js',
    'public/js/popper.min.js',
    'public/js/bootstrap.min.js',
    'public/js/jquery.easing.min.js',
    'public/js/swiper.min.js',
    'public/js/jquery.magnific-popup.js',
    'public/js/morphext.min.js',
    'public/js/isotope.pkgd.min.js',
    'public/js/validator.min.js',
    'public/js/scripts.js',
    'public/js/select2.min.js',
    'public/js/select2tree.js',
    'public/js/application-extra.js',
], 'public/js/editApplication.js');