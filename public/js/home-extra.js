$(".select-custom").select2({
           // theme: 'bootstrap'
        }); 

    var i = 0;
       
    $("#add").click(function(){
   
        ++i;
   
        $("#educationTable").append('<tr><td><input type="text" name="education['+i+'][board]" placeholder="Enter your Board" class="form-control" /></td><td><input type="text" name="education['+i+'][year]" placeholder="Enter your Year" class="form-control" /></td><td><input type="text" name="education['+i+'][percentage]" placeholder="Enter your Percentage" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
    });
    $(document).on('click', '.remove-tr', function(){  
         $(this).parents('tr').remove();
    }); 


    var j = 0;
       
    $("#add-new").click(function(){
   
        ++j;
         $("#techincalTable").append('<tr><td><input type="text" name="experience['+j+'][company]" placeholder="Enter your company" class="form-control" /></td><td><input type="text" name="experience['+j+'][designation]" placeholder="Enter your designation" class="form-control" /></td><td><input type="datetime-local" name="experience['+j+'][form]" placeholder="Enter your form" class="form-control" /></td><td><input type="datetime-local" name="experience['+j+'][to_date]" placeholder="Enter your to_date" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr-new">Remove</button></td></tr>');
    });
    $(document).on('click', '.remove-tr-new', function(){  
         $(this).parents('tr').remove();
    }); 



    


          $(".hindi-row").change(function() {
              var ischecked= $(this).is(':checked');
              if(!ischecked){
                var hindiRow = $('.hindi-read, .hindi-write, .hindi-speak').prop('checked', false);
                var hindiRow = $('.hindi-read, .hindi-write, .hindi-speak').prop("disabled", true);
              }else{
                var hindiRow = $('.hindi-read, .hindi-write, .hindi-speak').prop("disabled", false);
              }
              
          });


          $(".english-row").change(function() {
              var ischecked= $(this).is(':checked');
              if(!ischecked){
                var hindiRow = $('.english-read, .english-write, .english-speak').prop('checked', false);
                var hindiRow = $('.english-read, .english-write, .english-speak').prop("disabled", true);
              }else{
                var hindiRow = $('.english-read, .english-write, .english-speak').prop("disabled", false);
              }
          }); 

          $(".gujarati-row").change(function() {
              var ischecked= $(this).is(':checked');
              if(!ischecked){
                var hindiRow = $('.gujarati-read, .gujarati-write, .gujarati-speak').prop('checked', false);
                var hindiRow = $('.gujarati-read, .gujarati-write, .gujarati-speak').prop("disabled", true);
              }else{
                var hindiRow = $('.gujarati-read, .gujarati-write, .gujarati-speak').prop("disabled", false);
              }
          });


          $(".php").change(function() {
              var ischecked= $(this).is(':checked');
              if(!ischecked){
                var php = $('.php-beg, .php-med, .php-exp').prop("disabled", true);
                var php = $('.php-beg, .php-med, .php-exp').prop("checked", false);
              }else{
                var php = $('.php-beg, .php-med, .php-exp').prop("disabled", false);
              }
          }); 

          $(".mysql").change(function() {
              var ischecked= $(this).is(':checked');
              if(!ischecked){
                var mysql = $('.mysql-beg, .mysql-med, .mysql-exp').prop("disabled", true);
                var mysql = $('.mysql-beg, .mysql-med, .mysql-exp').prop("checked", false);
              }else{
                var mysql = $('.mysql-beg, .mysql-med, .mysql-exp').prop("disabled", false);
              }
          }); 

          $(".laravel").change(function() {
              var ischecked= $(this).is(':checked');
              if(!ischecked){
                var laravel = $('.laravel-beg, .laravel-med, .laravel-exp').prop("disabled", true);
                var laravel = $('.laravel-beg, .laravel-med, .laravel-exp').prop("checked", false);
              }else{
                var laravel = $('.laravel-beg, .laravel-med, .laravel-exp').prop("disabled", false);
              }
          }); 
          
           $(".oracle").change(function() {
              var ischecked= $(this).is(':checked');
              if(!ischecked){
                var oracle = $('.oracle-beg, .oracle-med, .oracle-exp').prop("disabled", true);
                var oracle = $('.oracle-beg, .oracle-med, .oracle-exp').prop("checked", false);
              }else{
                var oracle = $('.oracle-beg, .oracle-med, .oracle-exp').prop("disabled", false);
              }
          }); 